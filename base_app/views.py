from django.shortcuts import redirect, render

# Create your views here.

from django.shortcuts import render, redirect
from django.views.generic import TemplateView, FormView

class IndexView(TemplateView):
    def get(self, request, *args, **kwargs):
        return render(request, "index.html", { })

class PortfolioView(TemplateView):
    def get(self, request, *args, **kwargs):
        return render(request, "portfolio.html", { })
